var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'my site' });
});

router.get('/overmij', function(req, res, next) {
  res.render('overmij', { title: 'my site' });
});

router.get('/portfolio', function(req, res, next) {
  res.render('portfolio', { title: 'my site' });
});

router.get('/contact', function(req, res, next) {
  res.render('contact', { title: 'my site' });
});
module.exports = router;
